@extends('admin.leftsidebar')

@section('content')
    <div id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="add_category">
                        <h4>Add  Survey</h4>
                    </div>
                    <form action="{{route('admindashboard.store')}}"  method="post" enctype="multipart/form-data">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="exampleInputPassword1"> Survey</label>
                            <input type="text" name="cat" class="form-control" id="exampleInputPassword1" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Description</label>
                            <input type="text" name="description" class="form-control" id="exampleInputPassword1" placeholder="">
                        </div>

                        <button type="submit" class="btn btn-primary add_category_btn">Add</button>
                        {{csrf_field()}}
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

