@extends('admin.leftsidebar')

@section('content')
    <div class="row content_answer">
        <div class="content-wraper">
            <div class="container-fluid">
                <div class="row answer_form_content">
                    <div class="create_answer_form col-md-12">

                        <div class="alert alert-info" role="alert">
                            Create answers for  "{{$question->question}}" question
                        </div>
                        <form action="{{route('create-answer')}}" method="post" enctype="multipart/form-data">
                            <div class="form-group desk_label col-md-8">
                                <input type="hidden" name="question_id" value="{{$question->id}}">
                                <label for="exampleInputPassword1">Create Answer</label>

                                <div class="input-group mb-3">
                                    <input type="text" name="description" class="form-control" id="exampleInputPassword1" placeholder="">
                                    <div class="input-group-prepend">
                                        <button type="submit" class="input-group-text" id="basic-addon1 add_btn_answer">Add</button>
                                    </div>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>
                    @if($question->answers->count()>0)
                    <div class="added_answers">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Choices</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($question->answers as $key=>$answer)
                                    <tr>
                                        <th scope="row">{{$key+1}}</th>
                                        <td>{{$answer->description}}</td>
                                        <td>
                                            <a class="btn btn-danger" href="{{url("deleteanswer/".$answer->id)}}" onclick="return confirm('Are you sure?')">delete</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                    </div>
                    @else
                        <div class="clearfix  col-md-12">
                            <div class="alert alert-warning" role="alert">
                                There Is No Any Answers Yet
                            </div>
                        </div>
                    @endif


                </div>
            </div>


        </div>
    </div>
@endsection
