@extends('admin.leftsidebar')

@section('content')
    <div class="container" style="margin-top: 20px">
        <a  href="{{url('/addquestion')}}" class="btn btn-primary" >
            Add Question
        </a>
        <div class="alert alert-primary" role="alert">
            Create question for <a href="/showquestion/{{$question->id}}" class="alert-link">{{$question->cat}}</a>. category.
        </div>
        @if($question->questions->count() > 0)
        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>Question</th>
                <th>Type</th>
                <th>Delete</th>
                <th>Edit</th>
                <th>Add Answer</th>
                <th>Subquestion</th>
            </tr>
            </thead>
            <tbody>

                @foreach($question->questions_admin as $q)
                    {{--{{dd($question)}}--}}
                <tr>
                    <td> <a href="{{route('addanswer',$q->id)}}"><p class="survey_p answer">{{$q->question}}</p></a></td>
                    <td>{{$q->getCategoryName()}}</td>
                    <td>
                        <form action="{{url('/destroyquestion',$q->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Do You really want to delete this?<p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">Delete</button>
                        </form>
                    </td>
                    <td><a href="{{url('editquestion',$q->id)}}" class="btn btn-primary">Edit</a></td>
                    <td>
                        @if($q->type != \App\Question::QUSTOM_QUESTION )
                            <a href="{{url('addanswer',$q->id)}}" class="btn btn-success">Add Answer</a>
                        @endif
                    </td>
                    <td>
                        @if($q->type == \App\Question::WITH_SUBQUESTION)
                            <a href="{{url('addquestion',$q->id)}}" class="btn btn-success">Add Subquestion</a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <div class="clearfix  col-md-12">
                <div class="alert alert-warning" role="alert">
                    There Is No Any Questions Yet
                </div>
            </div>
            @endif
    </div>
@endsection
