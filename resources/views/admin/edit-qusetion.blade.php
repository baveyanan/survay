@extends('admin.leftsidebar')

@section('content')

<div id="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <form action="{{route('edit-question')}}"  method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="{{$question->id}}">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                            <label for="exampleFormControlSelect1">Please Select Survey</label>
                            <select class="form-control" id="exampleFormControlSelect1" name="cat_id">
                                @foreach($survey as $sur)
                                    <option value="{{$sur->id}}" {{ $question->cat_id == $sur->id ? "selected='selected'" : "" }} > {{{$sur->cat}}}</option>
                                @endforeach
                            </select>
                     </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Write Question Description</label>
                        <input type="text" name="question"  value="{{$question->question}}" class="form-control" id="exampleInputPassword1" placeholder="">
                    </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1"> Select Type Of Question</label><br>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" name="type" value="1" {{ $question->type == \App\Question::QUSTOM_QUESTION ? "checked='checked'" : "" }} class="form-check-input"> <label>Qastom Question</label>
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" name="type" value="2" {{ $question->type == \App\Question::WITH_SINGLE_CHOICE ? "checked='checked'" : "" }} class="form-check-input" > <label>With Single Choice </label>
                            </label>
                        </div>
                        <div class="form-check disabled">
                            <label class="form-check-label">
                                <input type="radio" name="type" value="3" {{ $question->type == \App\Question::WITH_MULTIPLE_CHOICE ? "checked='checked'" : "" }} class="form-check-input" > <label>With Multiple Choice</label>
                            </label>
                        </div>

                        </div>
                    <button type="submit" class="btn btn-primary">Edit Question</button>
                    {{csrf_field()}}
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
