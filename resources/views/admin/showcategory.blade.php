
@extends('admin.leftsidebar')

@section('content')

    <div class="container" style="margin-top: 20px">
        <a  href="{{url('/admindashboard/create')}}" class="btn btn-primary" >
            Add Survey
        </a>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Survey</th>
            <th>Description</th>
            <th>Delete</th>
            <th>Edit</th>
            <th>Show Questions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($survey as  $sur)
            <tr>
                <td>{{$sur->cat}}</td>
                <td>{{$sur->description}}</td>
                <td>
                    <form action="{{route('admindashboard.destroy',$sur->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        {{--<button class="btn btn-danger" type="submit">Delete</button>--}}
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Do You really want to delete this?<p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">Delete</button>
                    </form>
                </td>
                <td><a href="{{route('admindashboard.edit',$sur->id)}}" class="btn btn-primary">Edit</a></td>
                <td>  <a href="{{route('showquestion',$sur->id)}}"><p class="btn btn-success">Show Questions</p></a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
