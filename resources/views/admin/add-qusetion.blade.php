@extends('admin.leftsidebar')

@section('content')

<div id="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <form action="{{route('create-question')}}"  method="post" enctype="multipart/form-data">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(is_null($id))
                        <div class="form-group">
                                <label for="exampleFormControlSelect1">Please Select Survey</label>
                                <select class="form-control" id="exampleFormControlSelect1" name="cat_id" required {{ !is_null($id) ? "disabled" : "" }}>
                                    @foreach($survey as $sur)
                                        {{--{{dd($sur)}})--}}
                                        <option value="{{$sur->id}}">{{{$sur->cat}}}</option>
                                    @endforeach
                                </select>
                        </div>
                    @else
                       @if (!is_null($question) )
                            <input type="hidden" name="parent_id"  value="{{$question->id}}"   required>
                            <input type="hidden" name="cat_id"  value="{{$question->cat_id}}"  required>

                            <div class="alert alert-info">
                                    <strong>{{$question->question}}</strong>
                            </div>
                        @endif
                    @endif
                    <div class="form-group">
                        <label for="exampleInputPassword1">Write Question Description</label>
                        <input type="text" name="question" class="form-control" id="exampleInputPassword1" placeholder="" required>

                    </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1"> Select Type Of Question</label><br>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" name="type" value="1" class="form-check-input" required> <label>Qustom Question</label>
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" name="type" value="2" class="form-check-input" required> <label>With Single Choice </label>
                            </label>
                        </div>
                        <div class="form-check disabled">
                            <label class="form-check-label">
                                <input type="radio" name="type" value="3" class="form-check-input" required> <label>With Multiple Choice</label>
                            </label>
                        </div>

                        @if(is_null($id))
                                <div class="form-check disabled">
                                    <label class="form-check-label">
                                       <input type="radio" name="type" value="4" class="form-check-input" required> <label>Has Subquestions</label>
                                    </label>
                                </div>
                         @endif

                        </div>
                    <button type="submit" class="btn btn-primary">Add Question</button>
                    {{csrf_field()}}
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
