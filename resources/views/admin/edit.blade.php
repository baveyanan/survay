@extends('admin.leftsidebar')

@section('content')
    <div id="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <form action="{{route('admindashboard.update',$sur->id)}}"  method="post" enctype="multipart/form-data">
                            @method('PATCH')
                            @csrf
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="exampleInputPassword1"> Survey</label>
                                <input type="text" name="cat" class="form-control" id="exampleInputPassword1"  value="{{$sur->cat}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Description</label>
                                <input type="text" name="description" class="form-control" id="exampleInputPassword1" value = "{{$sur->description}}">
                            </div>
                                <button type="submit" class="btn btn-success">Update</button>
                            {{csrf_field()}}
                    </form>
                </div>
            </div>
        </div>
        <hr>
    </div>
@endsection

