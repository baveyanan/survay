@extends('admin.leftsidebar')

@section('content')
    <div class="survey_content">
        <table id="example" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>
                    survey
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($category as $cat)
                <tr>
                    <td>
                        <div class="survays_right_box_top">
                            <div class="survays_right_box_top_right"></div>
                            <a href="{{route('showquestion',$cat->id)}}"><p class="survey_p answer">{{$cat->cat}}</p></a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
