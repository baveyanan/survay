@include('layouts.header')
<div class="container m_top category_content ">
    <div class="row">
        <div class="col-md-12 ">
            <div class="filed_first">
                <p class="pat_name">Patient Name</p>
                <p>{{$patient ->first_name}} </p>
                <hr/>
                <p class="whatgender">What is your gender?</p>
                <div class="gender">
                    <p class="reg_male">{{$patient->gender}}</p>
                </div>
                <p class="whatbirth">What is your date of birth? (MM/DD/YYYY)</p>
                <p>{{$patient->date}}</p>
                <hr>
                <p class="whatheight">What is your height in inches? (6 foot = 72 inches)</p>
                <p>{{$patient->height}}</p>
                <hr>
                <p class="whatweight">What is your weight (lbs)?</p>
                <p>{{$patient->weight}}</p>

            </div>
            <h2>{{$answers->cat}}</h2>
            @foreach($answers->questanswers as $q)

                <p class="questions">{{$q->question}}</p>
                @foreach($q->useranswer as $user)
                    <p>
                        <label class="check_container">{{$user->answer}}</label>
                    </p>

                @endforeach

            @endforeach

        </div>
    </div>
</div>

@include('layouts.footer')

