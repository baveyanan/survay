@include('layouts.header')
<section class="survays"  >

    <div class="survays_left">
        <div class="survays_left_top">
            <h3>{{ Auth::user()->name }}</h3>
            <p>{{ Auth::user()->email }}</p>

        </div>
        <div class="survays_left_dash">
            <div class="dash_1">
            </div>
            <p>Dashboard</p>
        </div>
        <div class="survays_left_mid">
            <div class="dash_flex">
                <img src="{{asset('img/dash1.png')}}" alt="">
                <a href="{{route('surveydashboard')}}"><p>Survey History</p></a>
            </div>
            {{--<div class="dash_flex">--}}
                {{--<img src="{{asset('img/dash2.png')}}" alt="">--}}
                {{--<p>Settings</p>--}}
            {{--</div>--}}
            <div class="dash_flex">
                <img src="{{asset('img/dash3.png')}}" alt="">
                <p class="log_out_user"> <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a></p>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
        <div class="survays_left_dash">
            <div class="dash_1">
            </div>
        </div>
        <div class="survays_left_bottom">
            <a href="{{route('show-patientlist')}}"><p>Patients’ list</p></a>
            <a href="{{route('add-patient')}}"><button type="button" name="button">+ Add Patient</button></a>
        </div>
    </div>


    <div class="survays_right" >

        <div class="survays_right_top">
            <div class="survays_right_top_1">
                <img src="{{asset('img/sur_face.png')}}" alt="">
                <p>Clinic Name</p>
            </div>
            <div class="survays_right_top_2">
                <div class="survays_right_top_2_black">
                    <div class="survays_right_img">
                        <img src="{{asset('img/sur1.png')}}" alt="">
                        <p>Lorem Ipsum</p>
                    </div>
                    <div class="survays_right_img">
                        <img src="{{asset('img/sur2.png')}}" alt="">
                        <p>Lorem Ipsum</p>
                    </div>
                    <div class="survays_right_img">
                        <img src="{{asset('img/sur3.png')}}" alt="">
                        <p>Lorem Ipsum</p>
                    </div>
                </div>
                <div class="survays_right_top_2_bott">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aliquam aliquet vulputate vehicula. Donec hendrerit
                        ligula quis mi porttitor ullamcorper. Proin ac congue quam,
                        in viverra sem. Cras vestibulum congue sem in sodales.
                        Fusce eget purus bibendum, ornare nisl eget, consectetur ipsum.
                        Donec elementum bibendum aliquam..</p>
                </div>
            </div>

            <div class="survays_right_top_3" >
                <input type="text" name="cat" value=""  id="search_name" placeholder="Search">
               <img src="{{asset('img/search.png')}}" alt="">
                {{ csrf_field()}}
            </div>
        </div>
            <div id="products">
                <div class="survays_title">
                    <p>Surveys</p>
                </div>
                <div class="survays_right_bottom" >
                    @foreach($survey as $sur)
                        <div class="survays_right_box" >
                            <div class="survays_right_box_top">
                                <div class="survays_right_box_top_right"></div>
                                <a href="{{'surveydashboard/'.$sur->id}}"><p>{{$sur->cat}}</p></a>
                            </div>
                            <p class="survays_right_text">{{$sur->description}}</p>
                        </div>
                    @endforeach

                </div>
                {{$survey->links() }}
            </div>
    </div>

</section>

@include('layouts.footer')
<script>
    $(document).ready(function () {
        $('#search_name').change(function () {
            // alert('aaa');
            var inputval = $(this).val();
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('autocomplete.search')}}",
                method: "POST",
                data: {inputval: inputval, _token: _token},
                success: function (data){

                    if (data) {
                        $('#products').html(data)
                    }
                }

            });
            $(document).on('click', 'li', function () {
                $('#search_name').val($(this).text());
                $('#allpillList').fadeOut();
            });
        })


    })
</script>
