@include('layouts.header')
<div class="reg">
    <div class="reg_size ">
        <form method="post" action="{{route('create-patient')}}" id="myform">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @csrf
                <div class="filed_first wow slideInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
                    <p class="pat_name">Patient Name</p>
                    <input type="text" name="first_name" value="" placeholder="First name">
                    <input type="text" name="last_name" value="" placeholder="Last Name">
                    <hr/>
                    <p class="whatgender">What is your gender?</p>
                    <div class="gender">
                        <input type="radio" name="gender" value="male">
                        <p class="reg_male">Male</p>
                        <input type="radio" name="gender" value="female">
                        <p>Female</p>
                    </div>
                    <button type="button" name="button" class="btn_next" id="next_field">Next</button>
                </div>
                <hr>

                <div class="second_fields wow slideInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div class="animate_r2">
                        <p class="whatbirth">What is your date of birth? (MM/DD/YYYY)</p>
                        <input  id="datefield" name="date" type="date" max='2000-13-13' value="" >
                        <hr>
                        <p class="whatheight">What is your height in inches? (6 foot = 72 inches)</p>
                        <input type="number" name="height" value="" min =1>
                        <hr>
                        <p class="whatweight">What is your weight (lbs)?</p>
                        <input type="number" name="weight" value="" min =1>
                    </div>
                    <button type="submit" name="button" id="event" >Next</button>
                </div>
        </form>
    </div>
</div>
</section>
@include('layouts.footer')

