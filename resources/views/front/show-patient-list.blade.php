@include('layouts.header')
<section class="survays" >

    <div class="survays_left">
        <div class="survays_left_top">
            <h3>{{ Auth::user()->name }}</h3>
            <p>{{ Auth::user()->email }}</p>

        </div>
        <div class="survays_left_dash">
            <div class="dash_1">
            </div>
            <p>Dashboard</p>
        </div>
        <div class="survays_left_mid">
            <div class="dash_flex">
                <img src="{{asset('img/dash1.png')}}" alt="">
                <a href="{{route('surveydashboard')}}"><p>Survey History</p></a>
            </div>
            <div class="dash_flex">
                <img src="{{asset('img/dash3.png')}}" alt="">
                <p class="log_out_user"> <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a></p>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
        <div class="survays_left_dash">
            <div class="dash_1">
            </div>
        </div>
        <div class="survays_left_bottom">
            <a href="{{route('show-patientlist')}}"><p>Patients’ list</p></a>
            <a href="{{route('add-patient')}}"><button type="button" name="button">+ Add Patient</button></a>
        </div>
    </div>



<div class="survey_content">

    <table  class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>First_name</th>
            <th>Last_name</th>
            <th>Gender</th>
            <th>Date</th>
            <th>Height</th>
            <th>weight</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach($patient as $pat)
            <tr>
                <td>{{$pat->first_name}}</td>
                <td>{{$pat->last_name}}</td>
                <td>{{$pat->gender}}</td>
                <td>{{$pat->date}}</td>
                <td>{{$pat->height}}</td>
                <td>{{$pat->weight}}</td>
                <td>
                    <form action="{{url('/destroypatient',$pat->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Do You really want to delete this?<p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</section>
@include('layouts.footer')

