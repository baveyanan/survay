<div id="parentdiv">
    @if (Auth::check())
        <a href="{{route('surveydashboard')}}" class="content_img"> <img src="{{asset('img/left-arrow.png')}}"/></a>
    @endif
    <form action="{{route('create-answer')}}" method="post" enctype="multipart/form-data" id="myform1">
        {{csrf_field()}}
        @if(isset($question))
            @foreach($question as $quest)
                  @if($quest->type == \App\Question::WITH_SUBQUESTION)
                    <p>{{$quest->question}}</p>
                      @else
                        <span></span>
                  @endif
            @endforeach
        @endif
        <div class="survays_right_box_top wow slideInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
            <div class="survays_right_box_top_right"></div>
            <input type="hidden" id="question_id" name="question_id" value="{{$q['id']}}">
            <div class="survey_p answer"><label>{{$q['question']}}</label>
                @if($q['type'] == 2)
                    <div class="answers_items">
                        <div class="answers_column answers_row">
                            <div class="row">
                                @if(isset($q['answers']))
                                    @foreach($q['answers'] as $answer)
                                        <div class="filed_first wow slideInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
                                            <p><input type="radio" data-title="{{$answer->description}}" name="answer" value="{{$answer['id']}}">{{$answer['description']}}
                                                <input type="hidden"  id="new" value="2"></p>
                                        </div>
                                    @endforeach
                                @else
                                    <span></span>
                                @endif
                            </div>
                        </div>
                    </div>
                @elseif($q['type'] == 3)
                    <div class="answers_items">
                        <div class="answers_column answers_row">
                            <div class="row">
                                @if(isset($q->answers))
                                    @foreach($q->answers as $answer)
                                        <div class=" filed_first wow slideInLeft" data-wow-duration="1s"
                                             data-wow-delay="0.5s">
                                            <p><input type="checkbox" name="answer"  data-title="{{$answer->description}}" value="{{$answer->id}}" required>{{$answer->description}}
                                                <input type="hidden" name="type" id="new" value="3">
                                            </p>
                                        </div>
                                    @endforeach
                                @else
                                    <span></span>
                                @endif
                            </div>
                        </div>
                    </div>
                @elseif($q['type'] ==1)
                    <div class="col-md-3">
                        <p><input type="text" name="answer">
                            <input type="hidden"  id="new" value="1"></p>
                    </div>
                @else
                    <span></span>
                @endif
                <div id="remove-row">
                    <button type="button" id="btn-more" class="btn btn-primary " data-question="{{$cat_id}}" data-id="{{$id}}">Next</button>
                </div>
            </div>
                {{--{{dd($cat_id)}}--}}

        </div>
    </form>
</div>


