@include('layouts.header')

<div class="survays_right">
    {{--<div class="survays_right_top">--}}
        {{--<div class="survays_right_top_3">--}}
            {{--<input type="text" name="cat" value="{{$q}}" id="search_name" placeholder="Search">--}}
            {{--<img src="{{asset('img/search.png')}}" alt="">--}}
            {{--{{ csrf_field()}}--}}
            {{--<div id="allpillList"></div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="survays_title">
        <p>Surveys</p>
    </div>
    <div class="survays_right_bottom">
        @foreach($survey as $sur)
            <div class="survays_right_box">
                <div class="survays_right_box_top">
                    <div class="survays_right_box_top_right"></div>
                    <a href="{{'surveydashboard/'.$sur->id}}"> <p>{{$sur->cat}}</p></a>
                </div>
                <p class="survays_right_text">{{$sur->description}}</p>
            </div>
        @endforeach

    </div>
    {{$survey->links() }}
</div>
@include('layouts.footer')
