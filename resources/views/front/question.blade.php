@include('layouts.header')
@if (Auth::check())
    <a href="{{route('surveydashboard')}}" class="content_img"> <img src="{{asset('img/left-arrow.png')}}"/></a>
@endif
<div class="survays_answers survey_content survey_content_questions " id="parentdiv">
    <form action="{{route('create-answer')}}" method="post" enctype="multipart/form-data" id="myform1">
        {{csrf_field()}}
        @if(isset($question))
        @foreach($question as $quest)
            @if($quest->type == \App\Question::WITH_SUBQUESTION)
                <p>{{$quest->question}}</p>
                @else
                    <span></span>
            @endif
        @endforeach
        @endif
        @foreach($showanswer_all->question as $q)
            <div class=" content_survay survays_right_box_top wow slideInLeft " data-wow-duration="1s" data-wow-delay="0.5s" id="addnew">
                <input type="hidden" id="question_id" name="question_id" value="{{$q->id}}">
                <div class="survays_right_box_top_right"></div>
                {{--{{{dd($q->question)}}}}--}}
                <div class="survey_p answer"><label>{{$q->question}}</label>
                    @if($q->type == 2)
                        <div class="answers_items">
                            <div class="answers_column answers_row">
                                <div class="row">
                                    @if(isset($q->answers))
                                        @foreach($q->answers as $answer)
                                            <div class=" filed_first wow slideInLeft" data-wow-duration="1s"
                                                 data-wow-delay="0.5s">
                                                <p><input type="radio" name="answer" data-title="{{$answer->description}}" value="{{$answer->id}}" required>{{$answer->description}}
                                                    <input type="hidden" name="type" id="new" value="2">
                                                </p>
                                            </div>
                                        @endforeach
                                    @else
                                        <span></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @elseif($q->type == 3)
                        {{--dd()--}}
                        <div class="answers_items">
                            <div class="answers_column answers_row">
                                <div class="row">
                                    @if(isset($q->answers))
                                        @foreach($q->answers as $answer)
                                            <div class=" filed_first wow slideInLeft" data-wow-duration="1s"
                                                 data-wow-delay="0.5s">
                                                <p><input type="checkbox"  name="answer"  data-title="{{$answer->description}}" value="{{$answer->id}}" required>{{$answer->description}}
                                                    <input type="hidden" name="type" id="new" value="3">
                                                </p>
                                            </div>
                                        @endforeach
                                    @else
                                        <span></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @elseif($q->type ==1)
                        <div class="col-md-12 input_question">
                            <p><input type="text" name="answer" required>
                                <input type="hidden" id="new" value="1"></p>
                        </div>
                    @else
                        <span></span>
                    @endif
                        <div id="remove-row">
                            <button type="button" id='btn-more' class="btn btn-primary " data-question="{{$showanswer_all->id}}" data-id="0">Next</button>
                        </div>
                </div>
            </div>
        @endforeach
    </form>
</div>
<form action="{{route('create-patient-answer',$showanswer_all->id)}}" method="post" id="patient" enctype="multipart/form-data"
      class="select_form select_form_content wow slideInLeft text-center" data-wow-duration="1s" data-wow-delay="0.5s"  id="myform">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{csrf_field()}}
    <select class="form-control" name="patient_id">
        @foreach($patient as $pat)
                <option value="{{$pat->id}}">{{$pat->first_name." ".$pat->last_name}}</option>
        @endforeach
    </select>
    <button type="submit" class="btn btn-primary">Next</button>
</form>
@include('layouts.footer')

<script>
    $(document).on('click', '#btn-more', function () {
        $('.remove-div').remove();

        var bool = true;

        var val =$('#myform1').find('input');

        $.each( val, function( key, value ) {
            console.log($(value));
            if($(value).val() == ''){
                console.log('ggf');
                $(this).parents('div#parentdiv').append('<span  class ="remove-div" style="color: red;">This field is required. </span>');
                bool = false;
                return;
            }
        });
        // if(!bool){
        //     return false;
        // }
        //
        // $('#myform').submit();


        var cat_id = $(this).attr('data-question');
        // console.log(question_id);
        var id = $(this).data('id');
        var patient = $('input[name=patient_id]').val();
        var question_id = $('input[name=question_id]').val();
        var type = $('#new').val();
        var answer_id = 0;
        var value;
        var a = [];
        // console.log(type);
        switch (type) {
            case '1':
                value = $('input[name=answer]').val();
                console.log(value);
                break;
            case '2':

                answer_id = $('input[name=answer]:checked').val();
                value = $('input[name=answer]:checked').attr('data-title');

                break;
            case '3':
                value = $('input[name=answer]:checked').attr('data-title');

                $('input[name="answer"]:checked').each(function() {
                    a.push(this.value);
                });

                answer_id = a;
                break;
        }
        console.log(value,a,a.length);
        switch (type) {
            case '1':
            case '2':
                if(!value){
                    alert('Please select answer.');
                    return false;
                }
            break;
            case '3':
                if(a.length == 0){
                    alert('Please select answer.');
                    return false;
                }
            break;
        }
       // $('input[name="answer"]:checked').serialize()



        $('#myform').submit();

        $.ajax({
            url: '{{route('addajax')}}',
            method: 'post',
            data: {
                id: id,
                question_id:question_id,
                cat_id:cat_id,
                value: value,
                patient: patient,
                answer_id: answer_id,
                type: type,
            },
            headers: {
                'X-CSRF-Token': $("meta[name='csrf-token']").attr('content')
            },
            success: function (data) {
                value = '';
                if (data) {
                    if (data instanceof Object) {
                        if (data.success = 'ended') {
                            $('.survey_content_questions').html('');
                            $('#patient').show(200)
                        }
                    } else {
                        $('.survey_content_questions').html(data)
                    }
                }
            }
        });
    })
</script>
