<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Answer;

class Survey extends Model
{
    protected $fillable= ['cat','description'];

    public function question()
    {
        return $this->hasMany(Question::class,'cat_id')->where('type','!=',  Question::WITH_SUBQUESTION)->limit(1);
    }

    public function questions()
    {
        return $this->hasMany(Question::class,'cat_id')->where('type','!=',  Question::WITH_SUBQUESTION);
    }

    public function questions_admin()
    {
        return $this->hasMany(Question::class,'cat_id');
    }

    //qans
    public function questanswers()
    {
        return $this->hasMany(Question::class,'cat_id');
    }






}
