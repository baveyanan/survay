<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Answer;

class Question extends Model
{
    const QUSTOM_QUESTION = 1;
    const WITH_SINGLE_CHOICE = 2;
    const WITH_MULTIPLE_CHOICE = 3;
    const WITH_SUBQUESTION = 4;

    public $types = [
            self::QUSTOM_QUESTION=>'QUSTOM QUESTION',
            self::WITH_SINGLE_CHOICE=>'WITH SINGLE CHOICE',
            self::WITH_MULTIPLE_CHOICE=>'WITH MULTIPLE CHOICE',
            self::WITH_SUBQUESTION =>'HAS SUBQUESTION',

        ];

    protected $fillable =['question','cat_id','type','parent_id'];

    public function answers()
    {
        return $this->hasMany(Answer::class,'question_id');
    }
//kk

    public function useranswer()
    {
        return $this->hasMany(UserAnswer::class,'question_id');
    }

    public function subquestion()
    {
        return $this->hasMany(Question::class ,'id','parent_id');
    }


    public function getCategoryName()
    {
        return $this->types[$this->type];
    }


}
