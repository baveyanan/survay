<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{

    protected $fillable =['question_id','answer_id','answer','patient_id'];

    //qans
    public function getanswer()
    {
        return $this->belongsTo(Answer::class,'answer_id');
    }
}
