<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use App\Survey;
use Illuminate\Http\Request;
use Auth;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.dashboard');
    }

    public function create()
    {
        return view('admin.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'cat'=>'required',
        'description'=>'required',

        ]);
        $data = $request->all();
//        dd($data);
        $info = new Survey();
        $info ->fill($data);
        $info->save();
        return redirect('/showcat')->with('message','You have successfully created survey.');
    }


    public function edit($id)
    {
        $sur = Survey::find($id);
        return view('admin.edit',compact('sur'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'cat'=>'required',
            'description'=>'required',
        ]);
        $updatedata = Survey::find($id);
//        dd($request->all());
        $updatedata->cat = $request->cat;
        $updatedata->description = $request->description;
        $updatedata->save();

        return redirect('/showcat')->with('message','You have successfully edited survey.');
    }

    public function destroy($id)
    {
       $deletdata = Survey::find($id)->delete();
       return redirect()->back();
    }

    public function ShowCat()
    {
        $survey = Survey::orderBy('created_at')->get();
        return view('admin.showcategory',compact('survey'));
    }

    public function ShowAllCat()
    {
        $category = Survey::orderBy('created_at')->get();
        return view('admin.allcategory',compact('category'));
    }

//    question
    public function ShowQusetion($id)
    {
        $question = Survey::with('questions_admin')->where('id',$id)->first();
        return view('admin.question',compact('question'));
    }


    public function addQuestion($id = null)
    {
        $question = null;
        if (!is_null($id)){
            $question = Question::find($id);
        }
        $survey = Survey::orderBy('created_at','desc')->get();
      return view('admin.add-qusetion',['survey' => $survey,'id' => $id,'question' => $question]);
    }
//
    public function createQuestion(Request $request)
    {
        $request->validate([
            'question'=>'required',
        ]);
        $data =$request->all();
        $qestion = new Question();
        $qestion ->fill($data);
        $save = $qestion->save();
        return redirect('showquestion/'.$qestion->cat_id)->with('message','You have successfully added Question');
    }

//    answer
    public function addAnswer($id)
    {
        $question = Question::with('answers')->where('id',$id)->first();
        return view('admin.add-answer',compact('question'));
    }

    public function CreateAnswer(Request $request)
    {  $question_id = $request->question_id;
        $data = $request->all();
        $info = new Answer();
        $data['question_id']= $question_id;
        $info ->fill($data);
        $info->save();
        return redirect()->back()->with('message','You have successfully added answer.');
    }

    public function deleteAnswer($id){
        $deletdata = Answer::find($id)->delete();
        return redirect()->back()->with('message','You have successfully deleted answer.');
    }

    public function destroyQuestion($id){
        $deletdata = Question::find($id)->delete();
        return redirect()->back()->with('message','You have successfully deleted question.');
    }

    public function editQuestion($id)
    {
        $question =  Question::find($id);
        $survey = Survey::orderBy('created_at')->get();
        return view('admin.edit-qusetion',compact('survey','question'));
    }

    public function storeQuestion(Request $request)
    {
        $question =  Question::find($request->id);
        $question->cat_id = $request->cat_id;
        $question->question = $request->question;
        $question->type = $request->type;
        $question->save();
        return redirect()->back()->with('message','You have successfully edited question.');
    }


    public function addsubQuestion($id)
    {
        $question = Survey::with('questions')->where('id',$id)->first();
        $survey = Survey::orderBy('created_at')->get();
        return view('admin.add-qusetion',compact('survey','question'));
    }

}
