<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use App\Survey;
use App\Patient;
use App\UserAnswer;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use PDF;

class SurveyDashboadrController extends Controller
{
    public function index()
    {
        $survey = Survey::orderBy('created_at', 'desc')->paginate(6);
        if (isset(Auth::user()->id)){
            return view('front.surveydashboard', compact('survey'));
        } else {
            return redirect('/');
        }
    }
    public function Showpatientlist()
    {
        $patient = Patient::orderBy('created_at', 'asc')->get();
        return view('front.show-patient-list', compact('patient'));
    }

    public function destroyPatient($id)
    {
        $deletdata = Patient::find($id)->delete();
        return redirect()->back()->with('message','You have successfully deleted Patient.');
    }

    public function Questionform($id)
    {
        $patient = Patient::orderBy('created_at', 'asc')->get();
        $question = Question::where('type','!=',4)->with('subquestion')->get();
//        dd($question);
         $showanswer_all = Survey::with('question.answers')->where('id', $id)->first();
        return view('front.question', compact('showanswer_all', 'patient','question'));
    }

    public function ShowWithAjax(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $value = $request->value;
        $patient = $request->patient;
        $answer_id = $request->answer_id;
        $question_id = $request->question_id;


        $question = Question::with('subquestion')->get();

        $showanswer_all = Survey::with('questions.answers')->where('id', $request->cat_id)->first();


        $session = $request->session();
//        dd($session);

        if (isset($showanswer_all['questions'][$id])) {

            $answer = ($session->get('answer')) ? $session->get('answer') : [];

            if ($type == 1) {
                $answer[$id] = ['answer_id' => '', 'answer' => $value, 'patient' => $patient, 'question_id' => $question_id, 'type' => $type];
            } else {
                $answer[$id] = ['answer_id' => $answer_id, 'answer' => $value, 'patient' => $patient, 'question_id' => $question_id, 'type' => $type];
            }

            $request->session()->put('answer', $answer);
            $plus = $id + 1;
            if (isset($showanswer_all['questions'][$plus])) {
                return view('front.ques', ['q' => $showanswer_all['questions'][$plus], 'id' => $plus, 'cat_id' => $request->cat_id,'question'=>$question]);
            }
        };
        return Response()->json(['success' => 'ended']);
    }

    public function CreatePatientAnswer(Request $request, $id)
    {
        $pat = $request->all();
        $session = $request->session();
        $answer = $session->get('answer');

        if (Session::get('answer')) {
            foreach (Session::get('answer') as $answer){
                switch ($answer['type'] ){
                    case Question::QUSTOM_QUESTION;
                        $answers = [
                            'patient_id' => $pat['patient_id'],
                            'answer_id' => isset($answer['answer_id']) ? intval($answer['answer_id']) : null,
                            'answer' => $answer['answer'],
                            'question_id' => intval($answer['question_id']),
                        ];
                        UserAnswer::create($answers);

                    break;
                    case Question::WITH_SINGLE_CHOICE;
                        $answers = [
                            'patient_id' => $pat['patient_id'],
                            'answer_id' => isset($answer['answer_id']) ? intval($answer['answer_id']) : null,
                            'answer' => $answer['answer'],
                            'question_id' => intval($answer['question_id']),
                        ];
                        UserAnswer::create($answers);
                    break;
                    case Question::WITH_MULTIPLE_CHOICE;
                        if(!empty($answer['answer_id'])){
//                            dd($answer['answer_id']);
                            foreach ($answer['answer_id'] as $answer_id){

                                $answers = [
                                    'patient_id' => $pat['patient_id'],
                                    'answer_id' => $answer_id,
                                    'answer' => $answer['answer'],
                                    'question_id' => intval($answer['question_id']),
                                ];
                                UserAnswer::create($answers);
                            }
                        }

                    break;
                }
            }
//            dd($answers);
        }
        Session::forget('answer');

        return redirect()->route('show-answer-result', ['id' => $id, 'patient_id' => $pat['patient_id']]);
    }


    public function ShowAnsweRresult($id,$patient_id)
    {
        $patient = Patient::where('id',$patient_id)->first();
        $answers = Survey::with('questanswers.useranswer')->where('id', $id)->first();
//        dd($answers);
        return view('front.result', compact('answers', 'patient'));
    }

////    pdf
    public function downloadPDF($id,$patient_id)
    {
        $patient = Patient::where('id', $patient_id)->first();

        $answers = Survey::with('questanswers.useranswer')->where('id', $id)->first();
        $pdf = PDF::loadView('front.pdf', compact('answers', 'patient'));
        return $pdf->download('invoice.pdf');

    }

    // search
    public function AutocomplitSearch(Request $request)
    {
        $q = $request->get('inputval');
        $survey = Survey::orderBy('created_at', 'desc');
        if ($q) {
            $survey->where('surveys.cat', 'LIKE', "%{$q}%");
        }
        $survey = $survey->paginate(6);

        return view('front.showsearch-ajax', compact('survey', 'q'));
    }


}
