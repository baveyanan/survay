<?php

namespace App\Http\Controllers;

use App\Patient;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PatientController extends Controller
{
    public function ShowPatientsList()
    {
        return view('front.patients-list');
    }

    public function CreatePatientsList(Request $request)
    {
        $this->validate($request, [

            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'date' => 'required',
            'height' => 'required',
            'weight' => 'required',

        ]);

        $data = $request->all();
        $info = new Patient();
        $info ->fill($data);
        $info->save();
        Session::flash('message', 'You have successfully added the patient');
        return redirect()->route('surveydashboard');

    }

}
