<?php

namespace App;
use App\Survey;
use App\Question;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable =['description','question_id'];

}
