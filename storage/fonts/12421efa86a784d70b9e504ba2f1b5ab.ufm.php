<?php return array (
  'codeToName' => 
  array (
    32 => 'space',
    33 => 'exclam',
    34 => 'quotedbl',
    35 => 'numbersign',
    36 => 'dollar',
    37 => 'percent',
    38 => 'ampersand',
    39 => 'quotesingle',
    40 => 'parenleft',
    41 => 'parenright',
    42 => 'asterisk',
    43 => 'plus',
    44 => 'comma',
    45 => 'hyphen',
    46 => 'period',
    47 => 'slash',
    48 => 'zero',
    49 => 'one',
    50 => 'two',
    51 => 'three',
    52 => 'four',
    53 => 'five',
    54 => 'six',
    55 => 'seven',
    56 => 'eight',
    57 => 'nine',
    58 => 'colon',
    59 => 'semicolon',
    60 => 'less',
    61 => 'equal',
    62 => 'greater',
    63 => 'question',
    64 => 'at',
    65 => 'A',
    66 => 'B',
    67 => 'C',
    68 => 'D',
    69 => 'E',
    70 => 'F',
    71 => 'G',
    72 => 'H',
    73 => 'I',
    74 => 'J',
    75 => 'K',
    76 => 'L',
    77 => 'M',
    78 => 'N',
    79 => 'O',
    80 => 'P',
    81 => 'Q',
    82 => 'R',
    83 => 'S',
    84 => 'T',
    85 => 'U',
    86 => 'V',
    87 => 'W',
    88 => 'X',
    89 => 'Y',
    90 => 'Z',
    91 => 'bracketleft',
    92 => 'backslash',
    93 => 'bracketright',
    94 => 'asciicircum',
    95 => 'underscore',
    96 => 'grave',
    97 => 'a',
    98 => 'b',
    99 => 'c',
    100 => 'd',
    101 => 'e',
    102 => 'f',
    103 => 'g',
    104 => 'h',
    105 => 'i',
    106 => 'j',
    107 => 'k',
    108 => 'l',
    109 => 'm',
    110 => 'n',
    111 => 'o',
    112 => 'p',
    113 => 'q',
    114 => 'r',
    115 => 's',
    116 => 't',
    117 => 'u',
    118 => 'v',
    119 => 'w',
    120 => 'x',
    121 => 'y',
    122 => 'z',
    123 => 'braceleft',
    124 => 'bar',
    125 => 'braceright',
    126 => 'asciitilde',
    161 => 'exclamdown',
    162 => 'cent',
    163 => 'sterling',
    164 => 'currency',
    165 => 'yen',
    166 => 'brokenbar',
    167 => 'section',
    168 => 'dieresis',
    169 => 'copyright',
    170 => 'ordfeminine',
    171 => 'guillemotleft',
    172 => 'logicalnot',
    174 => 'registered',
    175 => 'macron',
    176 => 'degree',
    177 => 'plusminus',
    178 => 'twosuperior',
    179 => 'threesuperior',
    180 => 'acute',
    181 => 'mu',
    182 => 'paragraph',
    183 => 'periodcentered',
    184 => 'cedilla',
    185 => 'onesuperior',
    186 => 'ordmasculine',
    187 => 'guillemotright',
    188 => 'onequarter',
    189 => 'onehalf',
    190 => 'threequarters',
    191 => 'questiondown',
    192 => 'Agrave',
    193 => 'Aacute',
    194 => 'Acircumflex',
    195 => 'Atilde',
    196 => 'Adieresis',
    197 => 'Aring',
    198 => 'AE',
    199 => 'Ccedilla',
    200 => 'Egrave',
    201 => 'Eacute',
    202 => 'Ecircumflex',
    203 => 'Edieresis',
    204 => 'Igrave',
    205 => 'Iacute',
    206 => 'Icircumflex',
    207 => 'Idieresis',
    208 => 'Eth',
    209 => 'Ntilde',
    210 => 'Ograve',
    211 => 'Oacute',
    212 => 'Ocircumflex',
    213 => 'Otilde',
    214 => 'Odieresis',
    215 => 'multiply',
    216 => 'Oslash',
    217 => 'Ugrave',
    218 => 'Uacute',
    219 => 'Ucircumflex',
    220 => 'Udieresis',
    221 => 'Yacute',
    222 => 'Thorn',
    223 => 'germandbls',
    224 => 'agrave',
    225 => 'aacute',
    226 => 'acircumflex',
    227 => 'atilde',
    228 => 'adieresis',
    229 => 'aring',
    230 => 'ae',
    231 => 'ccedilla',
    232 => 'egrave',
    233 => 'eacute',
    234 => 'ecircumflex',
    235 => 'edieresis',
    236 => 'igrave',
    237 => 'iacute',
    238 => 'icircumflex',
    239 => 'idieresis',
    240 => 'eth',
    241 => 'ntilde',
    242 => 'ograve',
    243 => 'oacute',
    244 => 'ocircumflex',
    245 => 'otilde',
    246 => 'odieresis',
    247 => 'divide',
    248 => 'oslash',
    249 => 'ugrave',
    250 => 'uacute',
    251 => 'ucircumflex',
    252 => 'udieresis',
    253 => 'yacute',
    254 => 'thorn',
    255 => 'ydieresis',
    256 => 'Amacron',
    257 => 'amacron',
    258 => 'Abreve',
    259 => 'abreve',
    260 => 'Aogonek',
    261 => 'aogonek',
    262 => 'Cacute',
    263 => 'cacute',
    264 => 'Ccircumflex',
    265 => 'ccircumflex',
    266 => 'Cdotaccent',
    267 => 'cdotaccent',
    268 => 'Ccaron',
    269 => 'ccaron',
    270 => 'Dcaron',
    271 => 'dcaron',
    272 => 'Dcroat',
    273 => 'dmacron',
    274 => 'Emacron',
    275 => 'emacron',
    276 => 'Ebreve',
    277 => 'ebreve',
    278 => 'Edotaccent',
    279 => 'edotaccent',
    280 => 'Eogonek',
    281 => 'eogonek',
    282 => 'Ecaron',
    283 => 'ecaron',
    284 => 'Gcircumflex',
    285 => 'gcircumflex',
    286 => 'Gbreve',
    287 => 'gbreve',
    288 => 'Gdotaccent',
    289 => 'gdotaccent',
    290 => 'Gcommaaccent',
    291 => 'gcommaaccent',
    292 => 'Hcircumflex',
    293 => 'hcircumflex',
    294 => 'Hbar',
    295 => 'hbar',
    296 => 'Itilde',
    297 => 'itilde',
    298 => 'Imacron',
    299 => 'imacron',
    300 => 'Ibreve',
    301 => 'ibreve',
    302 => 'Iogonek',
    303 => 'iogonek',
    304 => 'Idot',
    305 => 'dotlessi',
    306 => 'IJ',
    307 => 'ij',
    308 => 'Jcircumflex',
    309 => 'jcircumflex',
    310 => 'Kcommaaccent',
    311 => 'kcommaaccent',
    312 => 'kgreenlandic',
    313 => 'Lacute',
    314 => 'lacute',
    315 => 'Lcommaaccent',
    316 => 'lcommaaccent',
    317 => 'Lcaron',
    318 => 'lcaron',
    319 => 'Ldot',
    320 => 'ldot',
    321 => 'Lslash',
    322 => 'lslash',
    323 => 'Nacute',
    324 => 'nacute',
    325 => 'Ncommaaccent',
    326 => 'ncommaaccent',
    327 => 'Ncaron',
    328 => 'ncaron',
    329 => 'napostrophe',
    330 => 'Eng',
    331 => 'eng',
    332 => 'Omacron',
    333 => 'omacron',
    334 => 'Obreve',
    335 => 'obreve',
    336 => 'Ohungarumlaut',
    337 => 'ohungarumlaut',
    338 => 'OE',
    339 => 'oe',
    340 => 'Racute',
    341 => 'racute',
    342 => 'Rcommaaccent',
    343 => 'rcommaaccent',
    344 => 'Rcaron',
    345 => 'rcaron',
    346 => 'Sacute',
    347 => 'sacute',
    348 => 'Scircumflex',
    349 => 'scircumflex',
    350 => 'Scedilla',
    351 => 'scedilla',
    352 => 'Scaron',
    353 => 'scaron',
    354 => 'Tcommaaccent',
    355 => 'tcommaaccent',
    356 => 'Tcaron',
    357 => 'tcaron',
    358 => 'Tbar',
    359 => 'tbar',
    360 => 'Utilde',
    361 => 'utilde',
    362 => 'Umacron',
    363 => 'umacron',
    364 => 'Ubreve',
    365 => 'ubreve',
    366 => 'Uring',
    367 => 'uring',
    368 => 'Uhungarumlaut',
    369 => 'uhungarumlaut',
    370 => 'Uogonek',
    371 => 'uogonek',
    372 => 'Wcircumflex',
    373 => 'wcircumflex',
    374 => 'Ycircumflex',
    375 => 'ycircumflex',
    376 => 'Ydieresis',
    377 => 'Zacute',
    378 => 'zacute',
    379 => 'Zdotaccent',
    380 => 'zdotaccent',
    381 => 'Zcaron',
    382 => 'zcaron',
    402 => 'florin',
    416 => 'Ohorn',
    417 => 'ohorn',
    431 => 'Uhorn',
    432 => 'uhorn',
    506 => 'Aringacute',
    507 => 'aringacute',
    508 => 'AEacute',
    509 => 'aeacute',
    510 => 'Oslashacute',
    511 => 'oslashacute',
    536 => 'Scommaaccent',
    537 => 'scommaaccent',
    710 => 'circumflex',
    711 => 'caron',
    728 => 'breve',
    729 => 'dotaccent',
    730 => 'ring',
    731 => 'ogonek',
    732 => 'tilde',
    733 => 'hungarumlaut',
    900 => 'tonos',
    901 => 'dieresistonos',
    902 => 'Alphatonos',
    903 => 'anoteleia',
    904 => 'Epsilontonos',
    905 => 'Etatonos',
    906 => 'Iotatonos',
    908 => 'Omicrontonos',
    910 => 'Upsilontonos',
    911 => 'Omegatonos',
    912 => 'iotadieresistonos',
    913 => 'Alpha',
    914 => 'Beta',
    915 => 'Gamma',
    917 => 'Epsilon',
    918 => 'Zeta',
    919 => 'Eta',
    920 => 'Theta',
    921 => 'Iota',
    922 => 'Kappa',
    923 => 'Lambda',
    924 => 'Mu',
    925 => 'Nu',
    926 => 'Xi',
    927 => 'Omicron',
    928 => 'Pi',
    929 => 'Rho',
    931 => 'Sigma',
    932 => 'Tau',
    933 => 'Upsilon',
    934 => 'Phi',
    935 => 'Chi',
    936 => 'Psi',
    938 => 'Iotadieresis',
    939 => 'Upsilondieresis',
    940 => 'alphatonos',
    941 => 'epsilontonos',
    942 => 'etatonos',
    943 => 'iotatonos',
    944 => 'upsilondieresistonos',
    945 => 'alpha',
    946 => 'beta',
    947 => 'gamma',
    948 => 'delta',
    949 => 'epsilon',
    950 => 'zeta',
    951 => 'eta',
    952 => 'theta',
    953 => 'iota',
    954 => 'kappa',
    955 => 'lambda',
    957 => 'nu',
    958 => 'xi',
    959 => 'omicron',
    960 => 'pi',
    961 => 'rho',
    963 => 'sigma',
    964 => 'tau',
    965 => 'upsilon',
    966 => 'phi',
    967 => 'chi',
    968 => 'psi',
    969 => 'omega',
    970 => 'iotadieresis',
    971 => 'upsilondieresis',
    972 => 'omicrontonos',
    973 => 'upsilontonos',
    974 => 'omegatonos',
    7808 => 'Wgrave',
    7809 => 'wgrave',
    7810 => 'Wacute',
    7811 => 'wacute',
    7812 => 'Wdieresis',
    7813 => 'wdieresis',
    7922 => 'Ygrave',
    7923 => 'ygrave',
    8211 => 'endash',
    8212 => 'emdash',
    8216 => 'quoteleft',
    8217 => 'quoteright',
    8218 => 'quotesinglbase',
    8220 => 'quotedblleft',
    8221 => 'quotedblright',
    8222 => 'quotedblbase',
    8224 => 'dagger',
    8225 => 'daggerdbl',
    8226 => 'bullet',
    8230 => 'ellipsis',
    8240 => 'perthousand',
    8249 => 'guilsinglleft',
    8250 => 'guilsinglright',
    8260 => 'fraction',
    8304 => 'zero.superior',
    8308 => 'four.superior',
    8309 => 'five.superior',
    8310 => 'six.superior',
    8311 => 'seven.superior',
    8312 => 'eight.superior',
    8313 => 'nine.superior',
    8317 => 'parenleft.superior',
    8318 => 'parenright.superior',
    8320 => 'zero.inferior',
    8321 => 'one.inferior',
    8322 => 'two.inferior',
    8323 => 'three.inferior',
    8324 => 'four.inferior',
    8325 => 'five.inferior',
    8326 => 'six.inferior',
    8327 => 'seven.inferior',
    8328 => 'eight.inferior',
    8329 => 'nine.inferior',
    8333 => 'parenleft.inferior',
    8334 => 'parenright.inferior',
    8363 => 'dong',
    8364 => 'Euro',
    8482 => 'trademark',
    8486 => 'Omega',
    8494 => 'estimated',
    8706 => 'partialdiff',
    8710 => 'increment',
    8719 => 'product',
    8721 => 'summation',
    8722 => 'minus',
    8730 => 'radical',
    8734 => 'infinity',
    8747 => 'integral',
    8776 => 'approxequal',
    8800 => 'notequal',
    8804 => 'lessequal',
    8805 => 'greaterequal',
    9674 => 'lozenge',
    57344 => 'G_tildecomb',
    57345 => 'M_uni0302',
    57346 => 'N_uni0302',
    57347 => 'f_f_j',
    57348 => 'f_j',
    57349 => 'g_tildecomb',
    57350 => 'm_uni0302',
    57351 => 'n_uni0302',
    57352 => 'zero.slashfitted',
    57732 => 'dong.alt',
    58112 => 'breve_acutecomb',
    58113 => 'breve_gravecomb',
    58114 => 'breve_hookabovecomb',
    58115 => 'breve_tildecomb',
    58116 => 'circumflex_acutecomb',
    58117 => 'circumflex_gravecomb',
    58118 => 'circumflex_hookabovecomb',
    58119 => 'circumflex_tildecomb',
    58120 => 'space_dotbelowcomb',
    58121 => 'space_hookabovecomb',
    58128 => 'space_uni031B',
    58129 => 'breve_acutecomb.cap',
    58130 => 'breve_gravecomb.cap',
    58131 => 'breve_hookabovecomb.cap',
    58132 => 'breve_tildecomb.cap',
    58133 => 'circumflex_acutecomb.cap',
    58134 => 'circumflex_gravecomb.cap',
    58135 => 'circumflex_hookabovecomb.cap',
    58136 => 'circumflex_tildecomb.cap',
    58137 => 'space_dotbelowcomb.cap',
    58144 => 'space_hookabovecomb.cap',
    58145 => 'space_uni031B.cap',
    61421 => 'dotaccent.cap',
    61422 => 'breve.cap',
    61425 => 'ogonek.cap',
    61426 => 'cedilla.cap',
    61427 => 'ring.cap',
    61429 => 'tilde.cap',
    61431 => 'circumflex.cap',
    63017 => 'cent.denominator',
    63018 => 'dollar.denominator',
    63019 => 'hyphen.denominator',
    63020 => 'parenleft.denominator',
    63021 => 'parenright.denominator',
    63022 => 'cent.numerator',
    63023 => 'dollar.numerator',
    63024 => 'hyphen.numerator',
    63025 => 'parenleft.numerator',
    63026 => 'parenright.numerator',
    63027 => 'at.cap',
    63028 => 'space_uni0326.cap',
    63032 => 'zero.slash',
    63033 => 'zero.fitted',
    63034 => 'two.fitted',
    63035 => 'three.fitted',
    63036 => 'four.fitted',
    63037 => 'five.fitted',
    63038 => 'six.fitted',
    63039 => 'seven.fitted',
    63040 => 'eight.fitted',
    63041 => 'nine.fitted',
    63042 => 'percent.oldstyle',
    63043 => 'zero.taboldstyle',
    63044 => 'one.taboldstyle',
    63045 => 'two.taboldstyle',
    63046 => 'three.taboldstyle',
    63047 => 'four.taboldstyle',
    63048 => 'five.taboldstyle',
    63049 => 'six.taboldstyle',
    63050 => 'seven.taboldstyle',
    63051 => 'eight.taboldstyle',
    63052 => 'nine.taboldstyle',
    63054 => 'Euro.taboldstyle',
    63055 => 'florin.taboldstyle',
    63056 => 'numbersign.taboldstyle',
    63057 => 'sterling.taboldstyle',
    63058 => 'yen.taboldstyle',
    63059 => 'dollar.taboldstyle',
    63060 => 'cent.taboldstyle',
    63061 => 'zero.denominator',
    63062 => 'one.denominator',
    63063 => 'two.denominator',
    63064 => 'three.denominator',
    63065 => 'four.denominator',
    63066 => 'five.denominator',
    63067 => 'six.denominator',
    63068 => 'seven.denominator',
    63069 => 'eight.denominator',
    63070 => 'nine.denominator',
    63071 => 'comma.denominator',
    63072 => 'period.denominator',
    63073 => 'zero.numerator',
    63074 => 'one.numerator',
    63075 => 'two.numerator',
    63076 => 'three.numerator',
    63077 => 'four.numerator',
    63078 => 'five.numerator',
    63079 => 'six.numerator',
    63080 => 'seven.numerator',
    63081 => 'eight.numerator',
    63082 => 'nine.numerator',
    63083 => 'comma.numerator',
    63084 => 'period.numerator',
    63150 => 'parenleft.cap',
    63151 => 'parenright.cap',
    63152 => 'bracketleft.cap',
    63153 => 'bracketright.cap',
    63154 => 'braceleft.cap',
    63155 => 'braceright.cap',
    63156 => 'exclamdown.cap',
    63157 => 'questiondown.cap',
    63158 => 'guillemotleft.cap',
    63159 => 'guillemotright.cap',
    63160 => 'guilsinglleft.cap',
    63161 => 'guilsinglright.cap',
    63162 => 'hyphen.cap',
    63163 => 'endash.cap',
    63164 => 'emdash.cap',
    63165 => 'periodcentered.cap',
    63169 => 'Scedilla.dup',
    63170 => 'scedilla.dup',
    63171 => 'space_uni0326',
    63177 => 'acute.cap',
    63178 => 'caron.cap',
    63179 => 'dieresis.cap',
    63180 => 'dieresis_acutecomb.cap',
    63181 => 'dieresis_gravecomb.cap',
    63182 => 'grave.cap',
    63183 => 'hungarumlaut.cap',
    63184 => 'macron.cap',
    63185 => 'breve.cyrcap',
    63186 => 'circumflex.cyrcap',
    63187 => 'space_uni030F.cap',
    63188 => 'breve.cyr',
    63189 => 'circumflex.cyr',
    63190 => 'space_uni030F',
    63191 => 'dieresis_acutecomb',
    63192 => 'dieresis_gravecomb',
    63196 => 'one.fitted',
    63199 => 'cent.inferior',
    63200 => 'cent.superior',
    63201 => 'comma.inferior',
    63202 => 'comma.superior',
    63203 => 'dollar.inferior',
    63204 => 'dollar.superior',
    63205 => 'hyphen.inferior',
    63206 => 'hyphen.superior',
    63207 => 'period.inferior',
    63208 => 'period.superior',
    63280 => 'zero.oldstyle',
    63281 => 'one.oldstyle',
    63282 => 'two.oldstyle',
    63283 => 'three.oldstyle',
    63284 => 'four.oldstyle',
    63285 => 'five.oldstyle',
    63286 => 'six.oldstyle',
    63287 => 'seven.oldstyle',
    63288 => 'eight.oldstyle',
    63289 => 'nine.oldstyle',
    64256 => 'f_f',
    64257 => 'fi',
    64258 => 'fl',
    64259 => 'f_f_i',
    64260 => 'f_f_l',
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'Myriad Pro',
  'FullName' => 'MyriadPro-Regular',
  'Version' => 'Version 2.062;PS 2.000;hotconv 1.0.57;makeotf.lib2.0.21895',
  'PostScriptName' => 'MyriadPro-Regular',
  'Weight' => 'Medium',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '50',
  'UnderlinePosition' => '-125',
  'FontHeightOffset' => '200',
  'Ascender' => '750',
  'Descender' => '-250',
  'FontBBox' => 
  array (
    0 => '-157',
    1 => '-250',
    2 => '1126',
    3 => '952',
  ),
  'StartCharMetrics' => '848',
  'C' => 
  array (
    32 => 212.0,
    33 => 230.0,
    34 => 337.0,
    35 => 497.0,
    36 => 513.0,
    37 => 792.0,
    38 => 605.0,
    39 => 188.0,
    40 => 284.0,
    41 => 284.0,
    42 => 415.0,
    43 => 596.0,
    44 => 207.0,
    45 => 307.0,
    46 => 207.0,
    47 => 343.0,
    48 => 513.0,
    49 => 513.0,
    50 => 513.0,
    51 => 513.0,
    52 => 513.0,
    53 => 513.0,
    54 => 513.0,
    55 => 513.0,
    56 => 513.0,
    57 => 513.0,
    58 => 207.0,
    59 => 207.0,
    60 => 596.0,
    61 => 596.0,
    62 => 596.0,
    63 => 406.0,
    64 => 737.0,
    65 => 612.0,
    66 => 542.0,
    67 => 580.0,
    68 => 666.0,
    69 => 492.0,
    70 => 487.0,
    71 => 646.0,
    72 => 652.0,
    73 => 239.0,
    74 => 370.0,
    75 => 542.0,
    76 => 472.0,
    77 => 804.0,
    78 => 658.0,
    79 => 689.0,
    80 => 532.0,
    81 => 689.0,
    82 => 538.0,
    83 => 493.0,
    84 => 497.0,
    85 => 647.0,
    86 => 558.0,
    87 => 846.0,
    88 => 571.0,
    89 => 541.0,
    90 => 553.0,
    91 => 284.0,
    92 => 341.0,
    93 => 284.0,
    94 => 596.0,
    95 => 500.0,
    96 => 300.0,
    97 => 482.0,
    98 => 569.0,
    99 => 448.0,
    100 => 564.0,
    101 => 501.0,
    102 => 292.0,
    103 => 559.0,
    104 => 555.0,
    105 => 234.0,
    106 => 243.0,
    107 => 469.0,
    108 => 236.0,
    109 => 834.0,
    110 => 555.0,
    111 => 549.0,
    112 => 569.0,
    113 => 563.0,
    114 => 327.0,
    115 => 396.0,
    116 => 331.0,
    117 => 551.0,
    118 => 481.0,
    119 => 736.0,
    120 => 463.0,
    121 => 471.0,
    122 => 428.0,
    123 => 284.0,
    124 => 239.0,
    125 => 284.0,
    126 => 596.0,
    160 => 212.0,
    161 => 230.0,
    162 => 513.0,
    163 => 513.0,
    164 => 513.0,
    165 => 513.0,
    166 => 239.0,
    167 => 519.0,
    168 => 300.0,
    169 => 677.0,
    170 => 346.0,
    171 => 419.0,
    172 => 596.0,
    173 => 291.0,
    174 => 419.0,
    175 => 300.0,
    176 => 318.0,
    177 => 596.0,
    178 => 311.0,
    179 => 305.0,
    180 => 300.0,
    181 => 553.0,
    182 => 512.0,
    183 => 207.0,
    184 => 300.0,
    185 => 244.0,
    186 => 355.0,
    187 => 419.0,
    188 => 759.0,
    189 => 759.0,
    190 => 759.0,
    191 => 406.0,
    192 => 612.0,
    193 => 612.0,
    194 => 612.0,
    195 => 612.0,
    196 => 612.0,
    197 => 612.0,
    198 => 788.0,
    199 => 585.0,
    200 => 492.0,
    201 => 492.0,
    202 => 492.0,
    203 => 492.0,
    204 => 239.0,
    205 => 239.0,
    206 => 239.0,
    207 => 239.0,
    208 => 671.0,
    209 => 658.0,
    210 => 689.0,
    211 => 689.0,
    212 => 689.0,
    213 => 689.0,
    214 => 689.0,
    215 => 596.0,
    216 => 689.0,
    217 => 647.0,
    218 => 647.0,
    219 => 647.0,
    220 => 647.0,
    221 => 541.0,
    222 => 531.0,
    223 => 548.0,
    224 => 482.0,
    225 => 482.0,
    226 => 482.0,
    227 => 482.0,
    228 => 482.0,
    229 => 482.0,
    230 => 773.0,
    231 => 447.0,
    232 => 501.0,
    233 => 501.0,
    234 => 501.0,
    235 => 501.0,
    236 => 234.0,
    237 => 234.0,
    238 => 234.0,
    239 => 234.0,
    240 => 541.0,
    241 => 555.0,
    242 => 549.0,
    243 => 549.0,
    244 => 549.0,
    245 => 549.0,
    246 => 549.0,
    247 => 596.0,
    248 => 549.0,
    249 => 551.0,
    250 => 551.0,
    251 => 551.0,
    252 => 551.0,
    253 => 471.0,
    254 => 569.0,
    255 => 471.0,
    256 => 612.0,
    257 => 482.0,
    258 => 612.0,
    259 => 482.0,
    260 => 612.0,
    261 => 482.0,
    262 => 580.0,
    263 => 448.0,
    264 => 580.0,
    265 => 448.0,
    266 => 580.0,
    267 => 448.0,
    268 => 580.0,
    269 => 448.0,
    270 => 666.0,
    271 => 574.0,
    272 => 671.0,
    273 => 564.0,
    274 => 492.0,
    275 => 501.0,
    276 => 492.0,
    277 => 501.0,
    278 => 492.0,
    279 => 501.0,
    280 => 492.0,
    281 => 501.0,
    282 => 492.0,
    283 => 501.0,
    284 => 646.0,
    285 => 559.0,
    286 => 646.0,
    287 => 559.0,
    288 => 646.0,
    289 => 559.0,
    290 => 646.0,
    291 => 559.0,
    292 => 652.0,
    293 => 555.0,
    294 => 657.0,
    295 => 555.0,
    296 => 239.0,
    297 => 234.0,
    298 => 239.0,
    299 => 234.0,
    300 => 239.0,
    301 => 234.0,
    302 => 239.0,
    303 => 234.0,
    304 => 239.0,
    305 => 234.0,
    306 => 609.0,
    307 => 477.0,
    308 => 370.0,
    309 => 243.0,
    310 => 542.0,
    311 => 469.0,
    312 => 469.0,
    313 => 472.0,
    314 => 236.0,
    315 => 472.0,
    316 => 236.0,
    317 => 472.0,
    318 => 244.0,
    319 => 472.0,
    320 => 272.0,
    321 => 476.0,
    322 => 242.0,
    323 => 658.0,
    324 => 555.0,
    325 => 658.0,
    326 => 555.0,
    327 => 658.0,
    328 => 555.0,
    329 => 555.0,
    330 => 658.0,
    331 => 555.0,
    332 => 689.0,
    333 => 549.0,
    334 => 689.0,
    335 => 549.0,
    336 => 689.0,
    337 => 549.0,
    338 => 894.0,
    339 => 863.0,
    340 => 538.0,
    341 => 327.0,
    342 => 538.0,
    343 => 327.0,
    344 => 538.0,
    345 => 327.0,
    346 => 493.0,
    347 => 396.0,
    348 => 493.0,
    349 => 396.0,
    350 => 493.0,
    351 => 396.0,
    352 => 493.0,
    353 => 396.0,
    354 => 497.0,
    355 => 331.0,
    356 => 497.0,
    357 => 339.0,
    358 => 497.0,
    359 => 331.0,
    360 => 647.0,
    361 => 551.0,
    362 => 647.0,
    363 => 551.0,
    364 => 647.0,
    365 => 551.0,
    366 => 647.0,
    367 => 551.0,
    368 => 647.0,
    369 => 551.0,
    370 => 647.0,
    371 => 551.0,
    372 => 846.0,
    373 => 736.0,
    374 => 541.0,
    375 => 471.0,
    376 => 541.0,
    377 => 553.0,
    378 => 428.0,
    379 => 553.0,
    380 => 428.0,
    381 => 553.0,
    382 => 428.0,
    402 => 513.0,
    416 => 689.0,
    417 => 553.0,
    431 => 677.0,
    432 => 551.0,
    506 => 612.0,
    507 => 482.0,
    508 => 788.0,
    509 => 773.0,
    510 => 689.0,
    511 => 549.0,
    536 => 493.0,
    537 => 396.0,
    538 => 497.0,
    539 => 331.0,
    540 => 480.0,
    541 => 427.0,
    562 => 541.0,
    563 => 471.0,
    710 => 300.0,
    711 => 300.0,
    713 => 300.0,
    728 => 300.0,
    729 => 300.0,
    730 => 300.0,
    731 => 300.0,
    732 => 300.0,
    733 => 300.0,
    894 => 217.0,
    900 => 301.0,
    901 => 346.0,
    902 => 615.0,
    903 => 217.0,
    904 => 561.0,
    905 => 722.0,
    906 => 309.0,
    908 => 728.0,
    910 => 707.0,
    911 => 742.0,
    912 => 236.0,
    913 => 612.0,
    914 => 542.0,
    915 => 451.0,
    916 => 620.0,
    917 => 492.0,
    918 => 553.0,
    919 => 652.0,
    920 => 689.0,
    921 => 239.0,
    922 => 542.0,
    923 => 609.0,
    924 => 804.0,
    925 => 658.0,
    926 => 540.0,
    927 => 689.0,
    928 => 634.0,
    929 => 532.0,
    931 => 552.0,
    932 => 497.0,
    933 => 588.0,
    934 => 718.0,
    935 => 571.0,
    936 => 685.0,
    937 => 705.0,
    938 => 239.0,
    939 => 588.0,
    940 => 551.0,
    941 => 440.0,
    942 => 555.0,
    943 => 236.0,
    944 => 524.0,
    945 => 551.0,
    946 => 560.0,
    947 => 464.0,
    948 => 546.0,
    949 => 440.0,
    950 => 409.0,
    951 => 555.0,
    952 => 534.0,
    953 => 236.0,
    954 => 493.0,
    955 => 472.0,
    956 => 553.0,
    957 => 469.0,
    958 => 433.0,
    959 => 549.0,
    960 => 555.0,
    961 => 563.0,
    962 => 417.0,
    963 => 554.0,
    964 => 413.0,
    965 => 524.0,
    966 => 642.0,
    967 => 457.0,
    968 => 655.0,
    969 => 694.0,
    970 => 236.0,
    971 => 524.0,
    972 => 549.0,
    973 => 524.0,
    974 => 694.0,
    1025 => 492.0,
    1026 => 665.0,
    1027 => 433.0,
    1028 => 576.0,
    1029 => 493.0,
    1030 => 239.0,
    1031 => 239.0,
    1032 => 370.0,
    1033 => 881.0,
    1034 => 897.0,
    1035 => 675.0,
    1036 => 542.0,
    1038 => 521.0,
    1039 => 635.0,
    1040 => 612.0,
    1041 => 546.0,
    1042 => 542.0,
    1043 => 433.0,
    1044 => 630.0,
    1045 => 492.0,
    1046 => 801.0,
    1047 => 492.0,
    1048 => 658.0,
    1049 => 658.0,
    1050 => 542.0,
    1051 => 594.0,
    1052 => 804.0,
    1053 => 652.0,
    1054 => 689.0,
    1055 => 639.0,
    1056 => 532.0,
    1057 => 580.0,
    1058 => 497.0,
    1059 => 521.0,
    1060 => 725.0,
    1061 => 571.0,
    1062 => 652.0,
    1063 => 597.0,
    1064 => 847.0,
    1065 => 864.0,
    1066 => 631.0,
    1067 => 746.0,
    1068 => 545.0,
    1069 => 568.0,
    1070 => 867.0,
    1071 => 543.0,
    1072 => 482.0,
    1073 => 528.0,
    1074 => 500.0,
    1075 => 385.0,
    1076 => 530.0,
    1077 => 501.0,
    1078 => 667.0,
    1079 => 431.0,
    1080 => 554.0,
    1081 => 554.0,
    1082 => 479.0,
    1083 => 507.0,
    1084 => 639.0,
    1085 => 546.0,
    1086 => 549.0,
    1087 => 541.0,
    1088 => 569.0,
    1089 => 448.0,
    1090 => 411.0,
    1091 => 471.0,
    1092 => 641.0,
    1093 => 463.0,
    1094 => 550.0,
    1095 => 515.0,
    1096 => 738.0,
    1097 => 749.0,
    1098 => 579.0,
    1099 => 665.0,
    1100 => 498.0,
    1101 => 464.0,
    1102 => 705.0,
    1103 => 496.0,
    1105 => 501.0,
    1106 => 557.0,
    1107 => 385.0,
    1108 => 460.0,
    1109 => 396.0,
    1110 => 234.0,
    1111 => 234.0,
    1112 => 243.0,
    1113 => 775.0,
    1114 => 782.0,
    1115 => 571.0,
    1116 => 479.0,
    1118 => 471.0,
    1119 => 538.0,
    1168 => 447.0,
    1169 => 392.0,
    1241 => 328.0,
    7808 => 846.0,
    7809 => 736.0,
    7810 => 846.0,
    7811 => 736.0,
    7812 => 846.0,
    7813 => 736.0,
    7840 => 612.0,
    7841 => 482.0,
    7842 => 612.0,
    7843 => 482.0,
    7844 => 612.0,
    7845 => 482.0,
    7846 => 612.0,
    7847 => 482.0,
    7848 => 612.0,
    7849 => 482.0,
    7850 => 612.0,
    7851 => 482.0,
    7852 => 612.0,
    7853 => 482.0,
    7854 => 612.0,
    7855 => 482.0,
    7856 => 612.0,
    7857 => 482.0,
    7858 => 612.0,
    7859 => 482.0,
    7860 => 612.0,
    7861 => 482.0,
    7862 => 612.0,
    7863 => 482.0,
    7864 => 492.0,
    7865 => 501.0,
    7866 => 492.0,
    7867 => 501.0,
    7868 => 492.0,
    7869 => 501.0,
    7870 => 492.0,
    7871 => 501.0,
    7872 => 492.0,
    7873 => 501.0,
    7874 => 492.0,
    7875 => 501.0,
    7876 => 492.0,
    7877 => 501.0,
    7878 => 492.0,
    7879 => 501.0,
    7880 => 239.0,
    7881 => 234.0,
    7882 => 239.0,
    7883 => 234.0,
    7884 => 689.0,
    7885 => 549.0,
    7886 => 689.0,
    7887 => 549.0,
    7888 => 689.0,
    7889 => 549.0,
    7890 => 689.0,
    7891 => 549.0,
    7892 => 689.0,
    7893 => 549.0,
    7894 => 689.0,
    7895 => 549.0,
    7896 => 689.0,
    7897 => 549.0,
    7898 => 689.0,
    7899 => 553.0,
    7900 => 689.0,
    7901 => 553.0,
    7902 => 689.0,
    7903 => 553.0,
    7904 => 689.0,
    7905 => 553.0,
    7906 => 689.0,
    7907 => 553.0,
    7908 => 647.0,
    7909 => 551.0,
    7910 => 647.0,
    7911 => 551.0,
    7912 => 677.0,
    7913 => 551.0,
    7914 => 677.0,
    7915 => 551.0,
    7916 => 677.0,
    7917 => 551.0,
    7918 => 677.0,
    7919 => 551.0,
    7920 => 677.0,
    7921 => 551.0,
    7922 => 541.0,
    7923 => 471.0,
    7924 => 541.0,
    7925 => 471.0,
    7926 => 541.0,
    7927 => 471.0,
    7928 => 541.0,
    7929 => 471.0,
    8208 => 307.0,
    8211 => 500.0,
    8212 => 1000.0,
    8216 => 207.0,
    8217 => 207.0,
    8218 => 207.0,
    8220 => 354.0,
    8221 => 354.0,
    8222 => 356.0,
    8224 => 500.0,
    8225 => 500.0,
    8226 => 282.0,
    8230 => 1000.0,
    8240 => 1156.0,
    8249 => 255.0,
    8250 => 255.0,
    8260 => 121.0,
    8304 => 336.0,
    8308 => 347.0,
    8309 => 308.0,
    8310 => 332.0,
    8311 => 294.0,
    8312 => 328.0,
    8313 => 327.0,
    8317 => 180.0,
    8318 => 180.0,
    8320 => 336.0,
    8321 => 244.0,
    8322 => 311.0,
    8323 => 305.0,
    8324 => 347.0,
    8325 => 308.0,
    8326 => 332.0,
    8327 => 294.0,
    8328 => 328.0,
    8329 => 327.0,
    8333 => 180.0,
    8334 => 180.0,
    8363 => 513.0,
    8364 => 513.0,
    8467 => 504.0,
    8470 => 916.0,
    8482 => 619.0,
    8486 => 705.0,
    8494 => 817.0,
    8706 => 375.0,
    8710 => 569.0,
    8719 => 615.0,
    8721 => 507.0,
    8722 => 596.0,
    8725 => 121.0,
    8729 => 207.0,
    8730 => 562.0,
    8734 => 728.0,
    8747 => 316.0,
    8776 => 596.0,
    8800 => 596.0,
    8804 => 596.0,
    8805 => 596.0,
    9674 => 522.0,
    57344 => 646.0,
    57345 => 804.0,
    57346 => 658.0,
    57347 => 809.0,
    57348 => 522.0,
    57349 => 559.0,
    57350 => 834.0,
    57351 => 555.0,
    57352 => 513.0,
    57732 => 513.0,
    58112 => 300.0,
    58113 => 300.0,
    58114 => 300.0,
    58115 => 300.0,
    58116 => 300.0,
    58117 => 300.0,
    58118 => 300.0,
    58119 => 300.0,
    58120 => 300.0,
    58121 => 300.0,
    58128 => 300.0,
    58129 => 300.0,
    58130 => 300.0,
    58131 => 300.0,
    58132 => 300.0,
    58133 => 300.0,
    58134 => 300.0,
    58135 => 300.0,
    58136 => 300.0,
    58137 => 300.0,
    58144 => 300.0,
    58145 => 300.0,
    61421 => 300.0,
    61422 => 300.0,
    61425 => 300.0,
    61426 => 300.0,
    61427 => 400.0,
    61429 => 300.0,
    61431 => 300.0,
    63017 => 315.0,
    63018 => 317.0,
    63019 => 191.0,
    63020 => 180.0,
    63021 => 180.0,
    63022 => 315.0,
    63023 => 317.0,
    63024 => 191.0,
    63025 => 180.0,
    63026 => 180.0,
    63027 => 737.0,
    63028 => 300.0,
    63032 => 513.0,
    63033 => 513.0,
    63034 => 447.0,
    63035 => 450.0,
    63036 => 503.0,
    63037 => 460.0,
    63038 => 515.0,
    63039 => 412.0,
    63040 => 513.0,
    63041 => 517.0,
    63042 => 818.0,
    63043 => 530.0,
    63044 => 530.0,
    63045 => 530.0,
    63046 => 530.0,
    63047 => 530.0,
    63048 => 530.0,
    63049 => 530.0,
    63050 => 530.0,
    63051 => 530.0,
    63052 => 530.0,
    63054 => 530.0,
    63055 => 530.0,
    63056 => 530.0,
    63057 => 530.0,
    63058 => 530.0,
    63059 => 530.0,
    63060 => 530.0,
    63061 => 336.0,
    63062 => 244.0,
    63063 => 311.0,
    63064 => 305.0,
    63065 => 347.0,
    63066 => 308.0,
    63067 => 332.0,
    63068 => 294.0,
    63069 => 328.0,
    63070 => 327.0,
    63071 => 131.0,
    63072 => 131.0,
    63073 => 336.0,
    63074 => 244.0,
    63075 => 311.0,
    63076 => 305.0,
    63077 => 347.0,
    63078 => 308.0,
    63079 => 332.0,
    63080 => 294.0,
    63081 => 328.0,
    63082 => 327.0,
    63083 => 131.0,
    63084 => 131.0,
    63150 => 284.0,
    63151 => 284.0,
    63152 => 284.0,
    63153 => 284.0,
    63154 => 284.0,
    63155 => 284.0,
    63156 => 230.0,
    63157 => 406.0,
    63158 => 419.0,
    63159 => 419.0,
    63160 => 255.0,
    63161 => 255.0,
    63162 => 307.0,
    63163 => 500.0,
    63164 => 1000.0,
    63165 => 207.0,
    63169 => 340.0,
    63170 => 268.0,
    63171 => 300.0,
    63177 => 300.0,
    63178 => 300.0,
    63179 => 300.0,
    63180 => 301.0,
    63181 => 301.0,
    63182 => 300.0,
    63183 => 300.0,
    63184 => 300.0,
    63185 => 300.0,
    63186 => 300.0,
    63187 => 300.0,
    63188 => 301.0,
    63189 => 300.0,
    63190 => 300.0,
    63191 => 301.0,
    63192 => 301.0,
    63196 => 326.0,
    63199 => 315.0,
    63200 => 315.0,
    63201 => 131.0,
    63202 => 131.0,
    63203 => 317.0,
    63204 => 317.0,
    63205 => 191.0,
    63206 => 191.0,
    63207 => 131.0,
    63208 => 131.0,
    63280 => 530.0,
    63281 => 362.0,
    63282 => 448.0,
    63283 => 433.0,
    63284 => 502.0,
    63285 => 441.0,
    63286 => 520.0,
    63287 => 428.0,
    63288 => 513.0,
    63289 => 520.0,
    64256 => 583.0,
    64257 => 523.0,
    64258 => 523.0,
    64259 => 815.0,
    64260 => 815.0,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJzt0gOwZscWxfG1Vo9t27Y9k4lt27Zt27Zt27Zt2373Td1XlbwkU5lJcieV/H9Vp/c5vXef3qe/T/qDiqqpumqopmqptuqoruqpvhqooRqpsZqoqZqpuVqopVqptdqordqpvTqoozqps7qoq7qpu3qop3qpt/qor/qpvwZooAZpsIZoqIZpuEZopEZptMZorMZpvCZooiZpsqZoNk3V7JpDc2ouza15NK/m0/xaQAtqIS2sRbSoFtPiWkJLaiktrWW0rJbT8lpBK2olrfxHP77CKlpVq2l1raE1tZbW1jpaV+tpfW2gDbWRNtYm2lSbaXNtoS21lbbWNtpW22l77aAdtZN21i7aVbtpd+2hPbWX9tY+2lf7aX8doAN1kA7WITpUh+lwHaEjdZSO1jE6VsfpeJ2gE3WSTtYpOlWn6XSdoTN1ls7WOTpX5+l8XaALdZEu1iW6VJfpcl2hK3WVrtY1ulbX6XrdoBt1k27WLbpVt+l23aE7dZfu1j26V/fpfj2gB/WQHtYjelSP6XE9oSf1lJ7WM3pWz+l5vaAX9ZJe1it6Va/pdb2hN/WW3tY7elfv6X19oA/1kT7WJ/pUn+lzfaEv9ZW+1jf6Vt/pe/2gHy3bcXE1V3cN13Qt13Yd13U913cDN3QjN3YTN3UzN3cLt3Qrt3Ybt3U7t3cHd3Qnd3YXd3U3d3cP93Qv93Yf93U/9/cAD/QgD/YQD/UwD/cIj/Qoj/YYj/U4j/cET/Sk3/cTe/J0clM823SyUz37jPyZqobn8Jyey3N7Hs87w2vn8/xewAt6IS/8u9cs4kVndJ9/Iy/mxSvGJaZbs6SX8tJexst6uarq65/Ky//kfgWv6JW8slfxql6t4nn1imsNr+m1vLbX8bpez+t7A2/ojbyxN/Gm3sybewtv6a28dUXlNt7W23l77+AdvZN39i7e1bt5d+/hPb2X9/Y+3tf7eX8f4AN9kA/2IT7Uh/lwH+EjfZSP9jE+1sf5eJ/gE32ST/YpPtWn+fQqOYUzfKbP8tk+x+f6PJ/vC3yhL/LFFZlLfKkv8+W+wlf6Kl/ta3ytr/P1vsE3+ibf7Ft8q2/z7b7Dd/ou3+17fK/v8/1+wA/6IT/sR/yoH/PjfsJP+ik/7Wf8rJ/z837BL/olv+xX/Kpf8+t+w2/6Lb/td/yu3/P7/sAf+iN/7E/8aUUXn/lzf+Ev/ZW/9jf+1t/5e//gH6Uo/qtPJ0n5q/eYMak2qzsAAAAAAAAAAAAAAAAAAAAAAAAAqkqqp0ZqplZqp85Mra+beqmfBmmYRmmcJmmaZmmeFmmZVmmdNmmbdmmfDumYTumcLumabumeHumZXumdPumbfumfARmYQRmcIRmaYRmeERmZURmdMRmbcRmfCZmYSZmcKZktUzN75sicmStzZ57Mm/kyfxbIglkoC2eRLJrFsniWyJJZKktnmSyb5bJ8VsiKWSkrZ5WsmtX+7DPEP1dWnzaukTUrn9fK2lmnIq6b9bJ+RdwgG2ajytzGv7J+k1/MbJrNfqVu8z+t5Z+/d4vKuGW2ytbZJttmu8qZ7bNDxbhjdsrO2SW7Zrfsnj2yZ/aqzO+dfWZ6132z3x/t/O8k+08bD/iN7IGV8aD/mz/4L2wJf5ocUhkP/UXmsIrr8Bwx7f7IyrmjcnTl3TG/+cZjZ7qX42aw/vjKeEJOnNk9AQD4O8hJs7oDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/E9Ozik5Nafl9JyRM3NWzp7VHQFVL+fM6g6Aqpdzc17OzwW5MBfl4lySS3PZz/KX54pcmatyda7Jtbku1+eGn+VvzE1V2jAAAAAAAAAAAMC/Vm7OLdPirbktt1fEOyquO2d1VwAAAAD+SXJX7s49uTf35f48kAfzUB7OI3m0MvtYHs8TeTJP5ek8k2fzXJ7PC3kxL+XlvJJX81pezxt5M29V1L6dd/Ju3sv7+SAf5qN8nE/yaT7L5/kiX+arfJ1v8m2+y/f5IT8WFZeUUqqV6qVGqTlrT+G/Sq1Su9QpdUu9Ur80KA1Lo9K4NClNS7PSvLQoLSurWpXWpc1PVrUt7Ur70qF0LJ1K59KldC3dSvfSo/QsvUrv0qeyqu+0sV/pXwaUgWVQGVyGlKFlWBle1V85fWVEGVlGldFlTBlbxpXxZUKZOKt7AgDg365MKpPLlDJbmTqrOwEAAEBV+A+33ouG',
  '_version_' => 6,
);