<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/','IndexController@show');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/surveydashboard', 'SurveyDashboadrController@index')->name('surveydashboard');
Route::get('/surveydashboard/{id}', 'SurveyDashboadrController@Questionform')->name('question-form');
Route::post('/createpatientanswer/{id}','SurveyDashboadrController@CreatePatientAnswer')->name('create-patient-answer');
Route::get('/showanswerresult/{id}/{patient_id}','SurveyDashboadrController@ShowAnsweRresult')->name('show-answer-result');

Route::get('/patientlist','SurveyDashboadrController@Showpatientlist')->name('show-patientlist');
Route::delete('/destroypatient/{id}', 'SurveyDashboadrController@destroyPatient')->name('destroypatientlist');

//search
Route::post('/search','SurveyDashboadrController@AutocomplitSearch')->name('autocomplete.search');

//pdf
Route::get('/downloadPDF/{id}/{patient_id}','SurveyDashboadrController@downloadPDF')->name('download-pdf');
// more data
Route::post('/addajax', 'SurveyDashboadrController@ShowWithAjax')->name('addajax');


Route::get('/patients', 'PatientController@ShowPatientsList')->name('add-patient');
Route::post('/patients', 'PatientController@CreatePatientsList')->name('create-patient');
Route::post('/createanswer','PatientController@CreateAnswer')->name('create-answer');



//admin
Route::group(['middleware' =>'admin'], function() {
    Route::resource('/admindashboard', 'AdminController');
    Route::get('/showcat', 'AdminController@ShowCat')->name('showcat');
    Route::get('/showallcat', 'AdminController@ShowAllCat')->name('showallcat');

//question
    Route::get('/showquestion/{id}', 'AdminController@ShowQusetion')->name('showquestion');
    Route::post('/addquestion', 'AdminController@createQuestion')->name('create-question');
    Route::get('/addquestion/{id?}', 'AdminController@addQuestion')->name('add-question');
//    subquest
    Route::get('/addquestion/{id}', 'AdminController@addsubQuestion');
    Route::get('/editquestion/{id}', 'AdminController@editQuestion')->name('edit-question');
    Route::post('/editquestion', 'AdminController@storeQuestion')->name('edit-question');
    Route::delete('/destroyquestion/{id}', 'AdminController@destroyQuestion')->name('destroyquestion');

////addansweradmin

    Route::get('/addanswer/{id}', 'AdminController@addAnswer')->name('addanswer');
    Route::get('/deleteanswer/{id}', 'AdminController@deleteAnswer')->name('deleteanswer');
    Route::post('/createanswer', 'AdminController@CreateAnswer')->name('create-answer');

});
